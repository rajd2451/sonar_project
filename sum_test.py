import unittest
from sum import sum

class SumTest(unittest.TestCase):

    def test_sumfunc_1(self):
        a = 10
        b = 20

        result = sum(a,b)

        self.assertEqual(result, a + b)


if __name__ == "__main__":
    unittest.main()